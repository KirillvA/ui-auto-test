var webdriver = require('selenium-webdriver');
var chrome    = require('selenium-webdriver/chrome');
var assert = require('assert');
var config = require('../config.json');
const screen = {
  width: 640,
  height: 480
};
const ELEMENT_WAIT_TIMEOUT = 40000;    
async function testLogin(driver){
  await driver.get('http://demo.it-serv.ru/armnext/demo/');
  const username = webdriver.By.name('username');
  await driver.wait(webdriver.until.elementLocated(username), ELEMENT_WAIT_TIMEOUT);
  await driver.findElement({name: 'username'}).sendKeys(config.login);
  await driver.findElement({name: 'password'}).sendKeys(config.password);
  await driver.findElement({className: 'x-btn-action-transparent-small'}).click();
  return driver.wait(webdriver.until.elementLocated( webdriver.By.className('user-list')), ELEMENT_WAIT_TIMEOUT);
}

describe('App test suite', function() {
  this.timeout(500000);

  var driver;

  beforeEach(function() {
    driver = new webdriver.Builder()
    .forBrowser('chrome')
    .setChromeOptions(new chrome.Options().headless().windowSize(screen))
    .build();
  });

  afterEach(function() {
    driver.quit();
  });

  it('Проверка логина', async function() {
    await testLogin(driver).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });

  it('Проверка фильтрации', async function() {
    await testLogin(driver);
    const User = webdriver.By.name('User');
    await driver.wait(webdriver.until.elementLocated(User), ELEMENT_WAIT_TIMEOUT);
    await driver.findElement({name: 'User'}).sendKeys(webdriver.Key.ENTER);
    driver.wait(webdriver.until.elementLocated( webdriver.By.tagName('table')), ELEMENT_WAIT_TIMEOUT).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });

  it('Проверка раздела задания', async function() {
    await testLogin(driver);
    
    await driver.get('http://demo.it-serv.ru/armnext/demo/#tasks');
    driver.wait(webdriver.until.elementLocated( webdriver.By.className('app-placeholder')), ELEMENT_WAIT_TIMEOUT).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });
  
  
  it('Проверка раздела администрирование', async function() {
    await testLogin(driver);
    
    await driver.get('http://demo.it-serv.ru/armnext/demo/#admin');
    
    driver.wait(webdriver.until.elementLocated( webdriver.By.className('app-users-grid')), ELEMENT_WAIT_TIMEOUT).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });

  it('Проверка раздела отчеты', async function() {
    await testLogin(driver);
    
    await driver.get('http://demo.it-serv.ru/armnext/demo/#report');
    driver.wait(webdriver.until.elementLocated( webdriver.By.tagName('ui')), ELEMENT_WAIT_TIMEOUT).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });

  it('Проверка раздела отчеты', async function() {
    await testLogin(driver);
    
    await driver.get('http://demo.it-serv.ru/armnext/demo/#report');
    driver.wait(webdriver.until.elementLocated( webdriver.By.tagName('ui')), ELEMENT_WAIT_TIMEOUT).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });

  it('Проверка раздела служебное', async function() {
    await testLogin(driver);
    
    await driver.get('http://demo.it-serv.ru/armnext/demo/#service');
    driver.wait(webdriver.until.elementLocated( webdriver.By.tagName('x-tab-bar')), ELEMENT_WAIT_TIMEOUT).then(
      function(){
        assert.ok(true);
      },
      function(){
        assert.fail(true);
      }
    );
  });
});
